var express = require('express');
const app = express();
var mongoose = require('mongoose');
var mongoDB = 'mongodb://127.0.0.1/mydb';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error :'));
app.use(express.json());
app.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*'); 
	response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    next();
});

app.get('/', (request, response) => {
    return response.send({ response: 'API response' });
})
// app.post('/api/login', (req,res) => {
//     if(req.email === user.email && req.password === user.password){
//         res.send(user.id)
//     } else 
// })
app.listen(1234, () => { console.log('Running'); });

